const BASE_URL = 'https://ajax.test-danit.com/api/swapi/films';
const root = document.querySelector('.root')

async function fetchFilms() {
  try {
    const response = await fetch(BASE_URL)
    const resultFetch = await response.json()
    return resultFetch

  } catch (e) {
    console.error(e)
  }
}


async function fetchCharacters(characters) {
  try {
    
    const elInfo = document.querySelector(".info")  

    for (let i = 0; i < characters.length; i++) {
      const actorUrl = characters[i]
      const responseURL = await fetch(actorUrl)
      const resultFetch = await responseURL.json()
      const actorName = resultFetch.name

      elInfo.insertAdjacentHTML('beforeend', ` ${ actorName} || `)
    }
    return charList

  } catch (e) {
    console.error(e)
  }
}



async function renderFilm(root, episodeId, name, openingCrawl) {
  try {
    root.insertAdjacentHTML('afterbegin', `<ol class="info">${episodeId} || ${name} || ${openingCrawl} </br> ACTORS: </ol>`)

  } catch (e) {
    console.error(e)
  }
}


window.addEventListener('DOMContentLoaded', async (event) => {
  event.preventDefault()
  const filmList = await fetchFilms()
 
  for (let i = 0; i < filmList.length; i++) {

    let { episodeId, name, openingCrawl, characters } = filmList[i];

    renderFilm(root, episodeId, name, openingCrawl)
    fetchCharacters(characters)
  }
})