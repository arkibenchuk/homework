var: 
1)При объявлении переменной var инициализация происходит в начале скрипта или функции (всплытие переменной, hoisting). hoisting происходит со значением "undefined".
2)Областью видимости переменной, объявленной через var, является её настоящий контекст выполнения.
3)Считается устаревшим ключевым словом.

let:

1)Имеет блочную видимость.
2)Значение переменной может изменятся.
3)Переменная let видна только после объявления. До этого момента ее нельзя вызвать.


Сonst:
1)Имеет блочную видимость.
2)Значение переменной не может изменятся.
3)переменная const также как и let не может быть вызванна до момента объявления.
4)При использовании const рекомендуется использовать ПРОПИСНЫЕ_БУКВЫ.
5)В const одновременно с объявлением переменной должно быть присвоено значение.


Объявление переменной через var было актуально до момента выхода ES6 который принес let и const. Они дополняют функционал переменной и их использование намного гибче чем объявление через var.

