class Employee {
    constructor(name, age, salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get salary(){
        return this._salary
    }
  
    get name(){
        return this._name;
    }

    get age(){
        return this._age;
    }

    set name(newName){
        this._name = newName.trim();
    }

    set salary(newSalary){
        return this._salary = newSalary;
    }
   
}




class Programmer extends Employee {
    constructor(name,age,salary,lang){
        super(name,age, salary)
        this._lang = lang;
    }
}


let dev1 = new Programmer("Arkadii", 22, 1000, ["html", "css", "js"]);
let dev2 = new Programmer("Ana", 24, 2000, ["c#", "python"]);


dev1.name = 'Ark'
dev1.salary *= 3;

dev2.name = 'Anabel'
dev2.salary *=3;