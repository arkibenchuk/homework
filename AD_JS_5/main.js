const elRoot = document.querySelector('.row')
const button = document.querySelector('.btn')

const serviceAPI = 'https://api.ipify.org/?format=json';
const ServiceIP = 'http://ip-api.com';
const commonFields ='fields=status,message,continent,country,regionName,city,district';





button.addEventListener('click', async (event) => {
	event.preventDefault()
	const IP = await findIP()
	const userAddress = await findAddress(IP)
	const { continent, country, regionName, city, district } = userAddress
	const renderAddress = await renderIP(continent,country,regionName,city,district,IP)
	elRoot.innerHTML = '';
	elRoot.insertAdjacentHTML('afterbegin', renderAddress)
})



async function findIP() {
	try {
		const responseURL = await fetch(serviceAPI)
		const resultFetch = await responseURL.json()
		
		return resultFetch.ip
	} catch (e) {
		console.error(e)
	}
}

async function findAddress(IP) {
	try {
		const responseURL = await fetch(
			ServiceIP + '/json/' + IP + '?' + commonFields
		)
		const resultFetch = await responseURL.json()

		return resultFetch
	} catch (e) {
		console.error(e)
	}
}

async function renderIP(continent, country, region, city, district,IP) {
	try {
		const addressContainer = 
		`<div class="col">Continent: ${continent}</div>
		<div class="col"> Country: ${country}</div>
		<div class="col"> Region: ${region}</div>
		<div class="col"> City: ${city}</div>
		<div class="col"> District: ${district}</div>
		<div class="col">IP address: ${IP}</div>`

		return addressContainer

	} catch (e) {
		console.error(e)
	}
}
