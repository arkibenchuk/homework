import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import './scss/variables.scss'

ReactDOM.render(
    <App/>,
    document.getElementById('root')
  );