import React, {useState, useEffect} from "react";
import {FaStar} from 'react-icons/fa';
import '../StarRating/star.scss'





const StarRating = (props) => {

  const starRatingLocalStorage = JSON.parse(localStorage.getItem(`rating ${props.name}`))

    useEffect(() => {
      localStorage.setItem(`rating ${props.name}`, JSON.stringify(rating))
    })


    const [rating, setRating] = useState(starRatingLocalStorage);
    const [hover, setHover] = useState(0);
    return (
      <>
        {[...Array(5)].map((star, index) => {
          index += 1;
          return (
            <button
              type="button"
              key={index}
              className={index <= (hover || rating) ? "on" : "off"}
              onClick={() => setRating(index)}
              onMouseEnter={() => setHover(index)}
              onMouseLeave={() => setHover(rating)}
            >
                <FaStar size={18} />
                
            </button>
          );
        })}
      </>
    );
  };

  export default StarRating
  