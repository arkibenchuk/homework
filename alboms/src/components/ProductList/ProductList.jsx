import React, {useEffect, useState} from "react";
import getAlboms from "../../Api/get";
import Card from '../Card/Card';
import PropTypes from 'prop-types'





const AlbomList = (props) => {

    let basketLocal = JSON.parse(localStorage.getItem('basketItems'))
    let favoriteLocal = JSON.parse(localStorage.getItem('favoriteItems'))

    let [basketItems, setBasketItems] = useState(
        basketLocal ? basketLocal : []
      )
    let [favoriteItems, setFavoriteItems] = useState(
        favoriteLocal ? favoriteLocal : []
      )

    let products = JSON.parse(localStorage.getItem('products'))
    

    useEffect(() => {
        localStorage.setItem('basketItems', JSON.stringify(basketItems))
      }, [basketItems])

    useEffect(() => {
        localStorage.setItem('favoriteItems', JSON.stringify(favoriteItems))
      }, [favoriteItems])
  


    const addToFavorites = (event) => {
        let cardId = event.target.closest('.music-card').id
        let card =  products.find(product => product.id == cardId);
        setFavoriteItems([...favoriteItems, card]);
    }

    const addToBasket = (event) => {
      let cardId = event.target.closest('.card-wrap').id
        let card =  products.find(product => product.id == cardId);
        setBasketItems([...basketItems, card]);
    }



    Card.propTypes = {
        name: PropTypes.string,
        src: PropTypes.string,  
        price: PropTypes.string,
        id: PropTypes.number
    }

    const [albom, setAlbom] = useState([])

    useEffect(() => {
            getAlboms().then((albom) => setAlbom(albom))}, [])

      useEffect(()=> {}, [albom])
      

       
      return(
          <>
            {albom.map(item => (
                <Card name={item.name}
                      price={item.price}
                      src={item.src}
                      key={item.id}
                      id={item.id}
                      albom={albom}
                      onClick={addToFavorites}
                      addToBasket={addToBasket}
                      />
                      
            ))}
          </>
      )
  }
  export default AlbomList