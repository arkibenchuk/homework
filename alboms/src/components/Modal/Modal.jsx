import React, { useEffect, useState } from "react";
import './modal.scss';
import Button from'../Button/Button';
import cross from '../Modal/cancel.svg';




const Modal = props => {


const infoFromLocalStorage = JSON.parse(localStorage.getItem('items'))   

    const [items, setItems] = useState(infoFromLocalStorage);
    
    useEffect(() => {
        localStorage.setItem('items', JSON.stringify(items))
    })

  

    return(
    
        <div className={`modal ${props.isOpened ? 'open' : 'close'}`} onClick={props.onModalClose}>
            <div className="modal-container"  style={{background:`${props.bgColor}`}} onClick={event=>{event.stopPropagation()}}>
                <div className={`modal-cross ${props.needCloseBtn ? 'show' : 'hide'}`} onClick={props.onModalClose}>
                    <img src={cross} width={'24px'} alt={'Close'}/>
                </div>
                <h3>{props.title}</h3>
                <hr/>

                    <p>{props.text}</p>

            <div className="modal-content">

                <Button name='OK' bgColor='Black' onClick={(e)=>{props.addToBasket(e); props.onModalClose(e)}}/>
                <Button name='Cancel' bgColor='Black' onClick={props.onModalClose}/>
                
                </div> 

            </div>
        </div>
    )
}

export default Modal