import React, {useState, useEffect} from "react";
import '../Card/card.scss';
import Button from '../Button/Button'
import Modal from '../Modal/Modal'
import StarRating from "../StarRating/StarRating";
import {FaStar} from "react-icons/fa"





const Card = (props) => {

    const [modal, setModal] = useState(false)
    const colorChange = (event) =>{
        const star = event.target.closest('.favorite-star')
        star.classList.toggle('active')
    }

    const addToBasket = props.addToBasket;

    return (
        <div id={props.id} className='card-wrap'>
        <div className='music-card' id={props.id}>

                <div className='card-content'>
                    
                 <div className='star-content'>
                <FaStar className='favorite-star' onClick={(e)=>{props.onClick(e); colorChange(e)}}/>
                
                </div>   
              
                    <div className='card-img'>
                        <img src={props.src} alt={props.name}/>
                    </div>

                    <div className='card-text'>
                        <h4 className='card-name'>{props.name}</h4>
                        <h4>{props.price}</h4>
                        <div><StarRating name={props.name} className='rating-star'/></div>
                        <Button name='Add to cart' bgColor='Black' onClick={()=> setModal(true)}/>
                    </div>
                </div>
  
                
           
           
            </div>


            <Modal title={`Do you want to buy ${props.name}`}
                            needCloseBtn={true}
                            isOpened={modal}
                            onModalClose = {() => setModal(false)}
                            addToBasket ={addToBasket}
                            />
            </div>
    )

        
}


export default Card