import React from "react";
import './button.scss'




const Button = props => {
  
  return <button className='btn' onClick={props.onClick} style={{background:`${props.bgColor}`}}>{props.name}</button>;

}

export default Button