import React, {useState, useEffect} from "react";
import '../FavoriteCard/favoriteCard.scss';
import StarRating from "../StarRating/StarRating";
import {FaStar} from "react-icons/fa"


const FavoriteCard = (props) => {

    const [modal, setModal] = useState(false)
  

    return (
        <>
        <div className='fmusic-card' id={props.id}>

                <div className='fcard-content'>
                    
                 <div className='fstar-content'>
                <FaStar className='favorite-star' onClick={(e)=>{props.onClick(e)}}/>
                
                </div>   
              
                    <div className='fcard-img'>
                        <img src={props.src} alt={props.name}/>
                    </div>

                    <div className='fcard-text'>
                        <h4 className='fcard-name'>{props.name}</h4>
                        <h4>{props.price}</h4>
                        <div><StarRating name={props.name} className='rating-star'/></div>
                        
                    </div>
                </div>
            </div>   
        </>    
    )

}


export default FavoriteCard