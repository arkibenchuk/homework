import React, {useState} from "react";
import { GrBasket } from 'react-icons/gr';
import {FaStar} from 'react-icons/fa'
import {BrowserRouter as Router, hashHistory, Link, Switch, Route} from "react-router-dom";


const Header = props => {

    return (
        <header>
                <h1 className='logo-title'><Link to='/' className='nav-link'>Alboms</Link></h1>
               
                <nav className='main-nav'>
                    <ul className='nav-list'>
                        <li className='nav-item'><Link to='/' className='nav-link'>Home</Link></li>
                        <li className='nav-item'>Cd's</li>
                        <li className='nav-item'>Register</li>
                    </ul>
                </nav>

              <div>
                    <Link to='/basket'><GrBasket className='basket-icon'/></Link>
              </div>

              <Link to='/favorites' className='nav-link'><FaStar className='star-icon'/></Link>
            </header>
    )
}


export default Header