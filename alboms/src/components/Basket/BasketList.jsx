import React from 'react';
import BasketCard from '../BasketCard/BasketCard'


const BasketList = (props) =>{

    let {basket, removeBasket} = props;
    return(
        <>
        <div className="bsection-wrapper">
          {basket.map(item => (
              <BasketCard name={item.name}
                    price={item.price}
                    src={item.src}
                    key={item.id}
                    id={item.id}
                    item={item}
                    removeBasket={removeBasket}
                    />
          ))}
          </div>
        </>
    )
}

export default BasketList