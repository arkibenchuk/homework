import '../Basket/basket.scss'
import React,{ useEffect, useState} from "react";
import BasketList from '../Basket/BasketList'

const Basket = () => {
    let [basketItems, setBasketItems] = useState(
        JSON.parse(localStorage.getItem('basketItems'))
      )
    let [basket, setBasket] = useState(basketItems ? basketItems : [])

    useEffect(() => {
        setBasket(basketItems)
    }, [])

    useEffect(() => {}, [basket])

    let removeBasket = (event) => {
        let removeItemId = event.target.closest('.bmusic-card').id
        let cardIndex = basket.findIndex(function (element){
            return element.id == removeItemId;
        })

        basket.splice(cardIndex, 1)
        localStorage.setItem('basketItems', JSON.stringify(basket))
        setBasketItems(JSON.parse(localStorage.getItem('basketItems')))
    }

   
    return (
        <div >
            <BasketList basket={basket} removeBasket={removeBasket}/>
        </div>
    )
}


export default Basket;