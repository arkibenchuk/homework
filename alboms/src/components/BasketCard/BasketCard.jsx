import React from "react";
import '../BasketCard/BasketCard.scss';
import StarRating from "../StarRating/StarRating";
import {FaStar} from "react-icons/fa"
import cross from '../BasketCard/cancel.svg';

const BasketCard = (props) => {


    return (
        <>
        <div className='bmusic-card' id={props.id}>

                <div className='bcard-content'>
                    
                 <div className='bstar-content'>
                <FaStar className='favorite-star' onClick={(e)=>{props.onClick(e)}}/>
                
                </div>   
                
                    <div className="close-wrap" onClick={(e)=>{props.removeBasket(e)}}>
                        <img src={cross} alt="X" width="20px" />
                    </div>
              
                    <div className='bcard-img'>
                        <img src={props.src} alt={props.name}/>
                    </div>

                    <div className='bcard-text'>
                        <h4 className='bcard-name'>{props.name}</h4>
                        <h4>{props.price}</h4>
                        <div><StarRating name={props.name} className='rating-star'/></div>
                        
                    </div>
                </div>
            </div>   
        </>    
    )

}


export default BasketCard