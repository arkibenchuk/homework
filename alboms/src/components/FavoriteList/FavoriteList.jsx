import React from 'react';
import FavoriteCard from '../FavoriteCard/FavoriteCard'


const FavoriteList = (props) =>{

    let {favorites, removeFavorites} = props;
    return(
        <>
        <div className="fsection-wrapper">
          {favorites.map(item => (
              <FavoriteCard name={item.name}
                    price={item.price}
                    src={item.src}
                    key={item.id}
                    id={item.id}
                    item={item}
                    onClick={removeFavorites}
                    />
          ))}
          </div>
        </>
    )
}


export default FavoriteList