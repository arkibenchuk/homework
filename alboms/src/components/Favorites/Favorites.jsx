import React,{ useEffect, useState} from "react";
import FavoriteList from '../FavoriteList/FavoriteList'

const Favorites = () => {
    let [favoriteItems, setFavoriteItems] = useState(
        JSON.parse(localStorage.getItem('favoriteItems'))
      )
    let [favorites, setFavorite] = useState(favoriteItems ? favoriteItems : [])

    useEffect(() => {
        setFavorite(favoriteItems)
    }, [])

    useEffect(() => {}, [favorites])

    let removeFavorites = (event) => {
        let removeItemId = event.target.closest('.fmusic-card').id
        let cardIndex = favorites.findIndex(function (element){
            return element.id == removeItemId;
        })

        favorites.splice(cardIndex, 1)
        localStorage.setItem('favoriteItems', JSON.stringify(favorites))
        setFavoriteItems(JSON.parse(localStorage.getItem('favoriteItems')))
    }

   
    return (
        <div >
            <FavoriteList favorites={favorites} removeFavorites={removeFavorites}/>
        </div>
    )
}


export default Favorites;