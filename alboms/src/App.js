import React from "react";
import Header from './components/Header/Header'
import './scss/variables.scss';
import './scss/main.scss';
import AlbomList from "./components/ProductList/ProductList";
import Favorites from './components/Favorites/Favorites'
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import Basket from "./components/Basket/Basket";

function App(){

    return (
        <Router>
     <div className='page-wrapper'>
        <Header />
        <section className='content' >
        
            <Switch>
                <Route exact path='/' component={AlbomList}/>
                <Route path='/favorites' component={Favorites}/>
                <Route path='/basket' component={Basket}/>
            </Switch> 
        </section>
    </div>
    </Router>

    )
}

export default App