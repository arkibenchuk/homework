let darkMode = localStorage.getItem('darkMode');
const modToggleBtn = document.getElementById('toggle-mod');
const elHeader = document.getElementById('header');
const elSection = document.querySelector('.container');




const enableDarkMode = () => {
    document.body.classList.add('dark-mode');
    elHeader.classList.add('dark-mode');
    elSection.style.background = 'url(img/night.jpg) center center';
    localStorage.setItem('darkMode', 'enabled');
};


const disableDarkMode = () => {
    document.body.classList.remove('dark-mode');
    elHeader.classList.remove('dark-mode');
    elSection.style.background = 'url(img/light.jpg) center center';
    localStorage.setItem('darkMode', null);
}


if (darkMode === "enabled"){
    enableDarkMode();
}

modToggleBtn.addEventListener('click', () => {

    darkMode = localStorage.getItem('darkMode');
    if (darkMode !== 'enabled'){
        enableDarkMode();
    }
    else{
        disableDarkMode();
    }
});