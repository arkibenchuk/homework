import React, { useEffect } from "react";
import './modal.scss';
import Button from'../Button/Button';
import cross from '../Modal/cancel.svg';





const Modal = props => {


  

    return(
    
        <div className={`modal ${props.isOpened ? 'open' : 'close'}`}>
            <div className="modal-container"  style={{background:`${props.bgColor}`}}>
                <div className={`modal-cross ${props.needCloseBtn ? 'show' : 'hide'}`} onClick={props.onModalClose}>
                    <img src={cross} width={'24px'}/>
                </div>
                <h3>{props.title}</h3>
                <hr/>
                
                    <p>{props.text}</p>

            <div className="modal-content">

                <Button name='Cancel' bgColor='Black'/>
                <Button name='OK' bgColor='Black'/>

                </div> 

            </div>
        </div>
    )
}

export default Modal