import React, {useState} from "react";
import Button from './components/Button/Button';
import Modal from './components/Modal/Modal';
import './scss/variables.scss';
import './scss/main.scss';





function App(){

const [modal, setModal] = useState({
    modal1: false,
    modal2: false
})


    return (

        <div className='container'>

            <Button name='Open first modal' bgColor='black'  onClick={() => setModal({...modal, modal1: true})}/>

            <Button name='Open second modal' bgColor='grey' onClick={() => setModal({...modal, modal2: true})}/>
        
            <Modal title='Do you want to delete this file?'
                    needCloseBtn={true}
                    isOpened={modal.modal1}
                    onModalClose = {() => setModal({...modal, modal1: false})}
                    text = 'Once you delete this file, you want be possible to undo this action.'
                    />

            <Modal title='Second Modal'
                    needCloseBtn={true}
                    isOpened={modal.modal2}
                    onModalClose = {() => setModal({...modal, modal2: false})}
                    text = 'some text here'
                    bgColor = 'grey'
                    />

        </div>
        
    )
}

export default App