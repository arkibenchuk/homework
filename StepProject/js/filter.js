document.querySelector('.nav-our-work').addEventListener('click', (event) =>{
    if (event.target.tagName !== 'LI') return false;
   
    let filterClass = event.target.dataset['filter'];

    filterBox.forEach( (item) => {
        item.classList.remove('hide')
        if(!item.classList.contains(filterClass) && filterClass != 'all'){
            item.classList.add('hide');
        }
    })

    document.querySelector('.nav-work-item.active').classList.remove('active');
    event.target.classList.add('active');
})




let loadResponse = [
  
    {
        img: "./img/graphicdesign/graphic-design6.jpg",
        title: "graphic"
    },

    {
        img: "./img/wordpress/wordpress3.jpg",
        title: "wordpress"
    },

    {
        img: "./img/graphicdesign/Layer 33.jpg",
        title: "graphicdesign"
    },
    {
        img: "./img/graphicdesign/Layer 25.jpg",
        title: "web"
    },

    {
        img: "./img/wordpress/wordpress4.jpg",
        title: "wordpress"
    },

    {
        img: "./img/landingpage/landing-page5.jpg",
        title: "landing"
    },

    {
        img: "./img/landingpage/landing-page6.jpg",
        title: "landing"
    },

    {
        img: "./img/landingpage/landing-page3.jpg",
        title: "landing"
    },
    
    {
        img: "./img/landingpage/landing-page1.jpg",
        title: "landing"
    },

    {
        img: "./img/wordpress/wordpress5.jpg",
        title: "wordpress"
    },

    {
        img: "./img/graphicdesign/Layer 25.jpg",
        title: "web"
    },

    {
        img: "./img/wordpress/wordpress4.jpg",
        title: "wordpress"
    },
];




const loadMoreBtn = document.getElementById('loadMore');
const filterBox = document.querySelectorAll('.box-item');

loadMoreBtn.addEventListener('click' , () =>{

    loadMoreBtn.style.backgroundColor = "transparent";
    loadMoreBtn.style.boxShadow = "none";
    loadMoreBtn.style.padding = "0";
    loadMoreBtn.style.borderRadius = "0";
    loadMoreBtn.innerHTML = `<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>   `;
 
    
    let ourGallery = document.querySelector('.our-work-gallery')


    setTimeout( () => {
        let responceList = loadResponse.splice(0,12);
        responceList.forEach((item) =>{
            let elDiv = document.createElement('div');
            elDiv.classList.add('work-gallery-item' , 'box-item', `${item.title}`);
            elDiv.innerHTML = `<div class="gallery-preview">
                                <h5>Creative design</h5>
                                <p>${item.title}</p>
                                </div>  
        <img src="${item.img}" alt="loadImg">`
        ourGallery.append(elDiv)
        })
        

        loadMoreBtn.innerHTML = 'add More'
        

      if (loadResponse.length === 0) {
        loadMoreBtn.style.display = 'none'
      }
    }, 3000)
}) 




        


