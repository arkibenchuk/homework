const servicesTab = document.querySelectorAll('.services-nav-list .services-nav-item');


for (let i = 0; i <= servicesTab.length - 1; i++) {
    let item = servicesTab[i]
    item.addEventListener('click', tabClickHandler)
  }
  

  function tabClickHandler(event) {
    document.querySelector('.services-nav-item.active').classList.remove('active')
    event.target.classList.add('active')
    document.querySelector('.services-item-block.active').classList.remove('active')
    const curentIndex = event.target.dataset.index
    document.getElementById('tab-' + curentIndex).classList.add('active');
  
  }
  



  function slowScroll (id){
    let offset = 0;

    $('html , body').animate ({
        scrollTop: $(id).offset ().top - offset
    }, 900);

    return false
}


let top_show = 440;

let delay = 1000; 


  $(document).ready(function() {
    $(window).scroll(function () { 
      
      if ($(this).scrollTop() > top_show) $('#scrollup').fadeIn();
      else $('#scrollup').fadeOut();
    });

    $('#scrollup').click(function () {
      $('body, html').animate({
        scrollTop: 0
      }, delay);
    });
  });


