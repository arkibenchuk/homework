let img = document.querySelectorAll('img');
let stopBtn = document.getElementById('stop');
let goBtn = document.getElementById('go');
let current = 0;



const reset = () => {

    for(let i = 0; i < img.length; i++) {
      img[i].style.display = "none";
    }
    }



 const start = () => {

    reset();
    if(current == img.length) {
      current = 0;
    }  
    img[current].style.display = "block";
    current++;
   
  }

  
let slideInter = setInterval(start, 1000);


stopBtn.addEventListener('click', () => {
    clearInterval(slideInter);
})


goBtn.addEventListener('click', ()=>{
    slideInter = setInterval(start, 1000)
})