import React from 'react';
import { useSelector} from 'react-redux';
import { FavoriteItem } from '../../components/favorite-item';
import './favorite-page.css';

export const FavoritePage = () => {
    const items = useSelector((state) => state.favorites.favoriteItems);

    if(items.length < 1) {
        return <h1>You have no favorite items</h1>
    }

    return (
        <div className="favorite-page">
            <div className="favorite-page__left">
                { items.map(game => <FavoriteItem game={game}/>)}
            </div>
           
        </div>
    )
}
