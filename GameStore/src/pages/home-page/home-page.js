import React, {useState, useEffect} from 'react';
import { GameItem } from '../../components/game-item';
import {getGames} from '../../Api/get'
import './home-page.css';



export const HomePage = () => {

    const [GAMES, setGAMES,] = useState([])
    useEffect(() => {getGames().then((GAMES) => setGAMES(GAMES));}, [])
    useEffect(()=> {}, [GAMES])

    return (
            <div className="home-page">
                { GAMES.map(game => <GameItem game={game} key={game.id}/>)}
            </div>
    )
}
