import React from 'react';
import { useSelector} from 'react-redux';
import { OrderItem } from '../../components/order-item';
import { calcTotalPrice, enumerate } from '../../components/utils';
import { Form } from '../../components/form'
import './order-page.css';

export const OrderPage = () => {
    const items = useSelector((state) => state.cart.itemsInCart);

    if(items.length < 1) {
        return <h1>Your cart is empty</h1>
    }

    return (
        <div className="order-page">
            <div className="order-page__left">
                <div>
                     <h5 className="order-page__sumtitle">{ items.length } { enumerate(items.length, ['item', 'items'])} for price ${calcTotalPrice(items)} </h5>
                </div>
                { items.map(game => <OrderItem game={game} key={game.id}/>)}
            </div>

            <div className="order-page__right"> 
                    <Form/>
            </div>
        </div>
    )
}
