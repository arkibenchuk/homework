import {
    Switch,
    Route,
  } from "react-router-dom";
import { HomePage } from "../../pages/home-page/home-page";
import { GamePage } from "../../pages/game-page";
import { OrderPage } from "../../pages/order-page";
import { FavoritePage } from "../../pages/favorite-page";


export const RoutePages = () => {
    return (
        <Switch>
              <Route exact path="/" component={HomePage}></Route>
              <Route exact path="/order" component={ OrderPage }></Route>
              <Route exact path="/app/:title" component={GamePage}></Route>
              <Route exact path="/favorites" component={FavoritePage}></Route>
            </Switch>
    )
}