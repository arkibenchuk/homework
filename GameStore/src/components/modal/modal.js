import React from "react";
import ReactDom from "react-dom";
import { useDispatch, useSelector } from "react-redux";
import './modal.css';
import { Button } from "../button";
import { setModal} from "../../store/modal/reducer"
import cross from '../modal/cancel.svg';
import { setItemInCart} from "../../store/cart/reducer";


export const Modal = () => {

    const modalState = useSelector((state)=> state.modal.isOpen)
    const currentGame = useSelector((state)=> state.games.currentGame)
    const dispatch = useDispatch();

    const handleCloseClick = () => {
        dispatch(setModal(false));
      };
    
    const handleAddtoCart = () => {
        dispatch(setItemInCart(currentGame))
        dispatch(setModal(false));
    }

    return ReactDom.createPortal(
        <>
        <div className={`modal ${modalState ? 'open' : 'close'}`} onClick={handleCloseClick} >
            <div className="modal-container" onClick={event=>{event.stopPropagation()}}>
                <div className={`modal-cross`} onClick={handleCloseClick} >
                    <img src={cross} width={'24px'} alt={'Close'}/>
                </div>
                <h3>{`Do you want add ${currentGame.title} to cart?`}</h3>
                <hr/>
            <div className="modal-content">
                <Button type="primary"  size="m" onClick={handleAddtoCart}>Add to cart</Button>
                <Button type="secondary" size="m" onClick={handleCloseClick}>Cancel</Button>
                </div>
            </div>
        </div>
        </>,
        document.getElementById('portal')
    )
}


