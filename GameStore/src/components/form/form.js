import React from 'react'
import { useDispatch, useSelector} from "react-redux";
import { Formik } from 'formik'
import *  as yup from 'yup'
import PhoneInput from 'react-phone-input-2'
import { clearCart } from "../../store/cart/reducer";
import 'react-phone-input-2/lib/style.css'
import './form.css'

export const Form = () => {

    const dispatch = useDispatch();
    const items = useSelector((state) => state.cart.itemsInCart);

    const validationSchema = yup.object().shape({
        name: yup.string().typeError('Must be a string').required('Required'),
        age: yup.number().typeError('Must be a number').required('Required'),
        shippingAddress: yup.string().typeError('Must be a string').required('Required'),
        phoneNumber: yup.number()
    })

   
    return (
        <Formik
            initialValues={{
                name: '',
                lastName: '',
                age: '',
                shippingAddress: '',
                phoneNumber: '',
            }}
            validateOnBlur
            onSubmit={(values, {resetForm}) => {
                console.log(values, items);
                resetForm({values:''})
                dispatch(clearCart())
            }}
            validationSchema={validationSchema}
            >

            {({values, errors, touched, handleChange, handleBlur, isValid, handleSubmit, dirty}) =>(
            <form className="order-page__form">
                <p>
                    <label htmlFor={'name'}> Name </label> <br/> 
                    <input 
                    type={'text'}
                    name={'name'}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.name}
                    />
                </p>
                { touched.name && errors.name && <p className={'error'}>{errors.name}</p> }
                <p>
                    <label htmlFor={'lastName'}> Last name </label> <br/> 
                    <input 
                    type={'text'}
                    name={'lastName'}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.lastName}
                    />
                </p>
                { touched.lastName && errors.lastName && <p className={'error'}>{errors.lastName}</p> }
                <p>
                    <label htmlFor={'age'}> Age </label> <br/> 
                    <input 
                    type={'number'}
                    name={'age'}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.age}
                    />
                </p>
                { touched.age && errors.age && <p className={'error'}>{errors.age}</p> }
                <p>
                    <label htmlFor={'shippingAddress'}> Shipping Address </label> <br/> 
                    <input 
                    type={'text'}
                    name={'shippingAddress'}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.shippingAddress}
                    />
                </p>
                { touched.shippingAddress && errors.shippingAddress && <p className={'error'}>{errors.shippingAddress}</p> }
                
                    <label htmlFor={'phoneNumber'}>Phone</label> <br/> 
                    <PhoneInput>
                        <input   
                            type={'tel'}
                            name={'phoneNumber'}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.phoneNumber}/>
                    </PhoneInput>
               
                { touched.phoneNumber && errors.phoneNumber && <p className={'error'}>{errors.phoneNumber}</p> }
                <button
                    disabled={!isValid && !dirty}
                    onClick={handleSubmit} 
                    type={'submit'}
                    className={'order-page__submit'}
                >Submit</button>
            </form>
            )}
     </Formik>
    )
}
