import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button } from "../button";
import { deleteItemFromCart } from "../../store/cart/reducer";
import { setCurrentGame } from "../../store/games/reducer";
import { setModal} from "../../store/modal/reducer"
import "./game-buy.css";

export const GameBuy = ({ game }) => {
  const dispatch = useDispatch();
  const items = useSelector((state) => state.cart.itemsInCart);
  const isItemInCart = items.some((item) => item.id === game.id);

  
  const handleClick = (e) => {
    e.stopPropagation();
    if (isItemInCart) {
      dispatch(deleteItemFromCart(game.id));
     
    } else {
      dispatch(setCurrentGame(game))
      dispatch(setModal(true))
    }
  };

  return (
    <div className="game-buy">
      <span className="game-buy__price"> ${game.price}</span>
      <Button
        type={isItemInCart ? "secondary" : "primary" }
        onClick={handleClick}
      >
        {isItemInCart ? "Remove from Cart" : "Add to Cart"}
      </Button>
    </div>
  );
};
