import React from 'react';
import { useDispatch } from 'react-redux';
import { AiOutlineCloseCircle } from "react-icons/ai";
import { GameCover } from '../game-cover/game-cover';
import './favorite-item.css';
import { deleteFavorites } from '../../store/favorites/reducer';

export const FavoriteItem = ({ game }) => {
    const dispatch = useDispatch();
    const handleDeleteClick = () => {
        dispatch(deleteFavorites(game.id))
    }
    return (
        <div className="favorite-item">
            <div className="favorite-item__cover">
                <GameCover image={ game.image }/>
            </div>
            <div className="favorite-item__title">
                <span> { game.title } </span>
            </div>
            <div className="favorite-item__price">
                <span>${ game.price }</span>
                <AiOutlineCloseCircle
                    size={25}
                    className="cart-item__delete-icon"
                    onClick={handleDeleteClick}
                />
            </div>
        </div>
    )
}
