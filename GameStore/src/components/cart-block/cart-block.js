import React, {useCallback } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { BiCartAlt } from "react-icons/bi";
import { ItemsInCart } from "../items-in-cart";
import { calcTotalPrice } from '../utils';
import "./cart-block.css";

export const CartBlock = () => {
  const items = useSelector((state) => state.cart.itemsInCart);
  const history = useHistory();
  const totalPrice = calcTotalPrice(items);
  const handleGoToOrderClick = useCallback(() => {
    history.push('/order');
  }, [history]);

  return (
    <div className="cart-block">
      <ItemsInCart quantity={items.length}/>
      <BiCartAlt
        color="white"
        size={25}
        className="cart-icon"
        onClick={handleGoToOrderClick}
      />
      {totalPrice > 0 ? (
        <span className="total-price">${totalPrice}</span>
      ) : null}
    </div>
  );
};
