import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { FaStar } from "react-icons/fa"
import { setFavoriteItems, deleteFavorites } from "../../store/favorites/reducer";
import { Button } from "../button";


export const StarIcon = ({ game }) => {
  const dispatch = useDispatch();
  const items = useSelector((state) => state.favorites.favoriteItems);
  const isItemInFavorites = items.some((item) => item.id === game.id);

  const handleFavoritesClick = (e) => {
    e.stopPropagation();
    if (isItemInFavorites) {
      dispatch(deleteFavorites(game.id));
    } else {
      dispatch(setFavoriteItems(game));
    }
  };
  return (
    
  <Button onClick={handleFavoritesClick} type={'transparent'}>
        <FaStar size={25}  className="star" color={isItemInFavorites?'orange': 'white'}/>
    </Button>
  ); 
    
};
