import React from "react";
import { Link } from "react-router-dom";
import { CartBlock } from "../cart-block";
import "./header.css";

export const Header = () => {
  return (
    <div className="header">
      <div className="wrapper">
        <Link to="/" className="header__store-title">
          Game Store
        </Link>
      </div>

      <div className="header__nav">
        <li><Link to="/favorites" className="header__store-title">Favorites</Link></li>
        <CartBlock />
      </div>

    </div>
  );
};
