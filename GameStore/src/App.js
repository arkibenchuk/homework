import { Header } from "./components/header";
import { Provider } from "react-redux";
import { store } from "./store";
import {BrowserRouter as Router} from "react-router-dom";
import { RoutePages } from './components/route-pages/route-pages'
import { Modal } from '../src/components/modal/modal'

 
function App() {
  return (
    <Provider store={ store }>
      <Router>
        <div className="App">
          <Header />
            <RoutePages/>
        </div>
      </Router>

      <Modal/>
    </Provider>
  );
}

export default App;
