import { createSlice } from '@reduxjs/toolkit'


const initialState = JSON.parse(localStorage.getItem('')) || {
  currentGame: []
}


const gamesSlice = createSlice({
  name: 'games',
  initialState,
  reducers: {
    setCurrentGame: (state, action) => {
      state.currentGame = action.payload
    },
  }
});

export const { setCurrentGame } = gamesSlice.actions;
export default gamesSlice.reducer; 