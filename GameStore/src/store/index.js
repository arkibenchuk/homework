import { configureStore } from '@reduxjs/toolkit';
import gamesReducer from './games/reducer';
import cartReducer from './cart/reducer';
import favoriteReducer from './favorites/reducer';
import modalReducer from './modal/reducer';


function syncWithLocalStorage({ getState }) {
    return (next) => (action) => {
      const updatedAction = next(action)
      localStorage.setItem('redux', JSON.stringify(getState()))
      return updatedAction
    }
  }

export const store = configureStore({
    reducer: {
        games: gamesReducer,
        cart: cartReducer,
        favorites: favoriteReducer,
        modal: modalReducer,
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(syncWithLocalStorage)
  })

